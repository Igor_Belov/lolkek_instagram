class Post < ApplicationRecord
  
  belongs_to :user
  has_many :comments
  accepts_nested_attributes_for :comments
  has_many_attached :images

  validates :title, presence: true, length: { minimum: 2 }
  validates :content, presence: true, length: { minimum: 2 }
end
