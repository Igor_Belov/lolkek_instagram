# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
@admin = User.create(email: "admin@admin.admin", password: "password")
@admin.add_role "admin"

15.times do |n|
	user = User.create(email: "example#{n+1}@example.example", password: "password#{n+1}")
	user.add_role "user"
end

50.times do |n|
	Post.create(title: Faker::Lorem.sentence, content: Faker::Lorem.paragraph, user_id: rand(2..15))
end

50.times do |n|
	Comment.create(body: Faker::Lorem.paragraph, post_id: rand(1..10), user_id: rand(2..15))
end
